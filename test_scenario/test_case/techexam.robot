*** Settings ***
Suite Setup       Common.Suite Setup
Suite Teardown    Common.Suite Teardown
Test Setup        Common.Test Setup
Test Teardown     Common.Test Teardown
Library           SeleniumLibrary
Resource    ../Resources/Locator.robot


*** Test Cases ***
Page Traversal
    Open Browser To Home Page
    Go to About and Request Demo
    
Form Submission
    Open Browser To Home Page
    Go to About and Request Demo
    Click Submit Button
    Verify Page redirection after submit
    

*** Keywords ***
Open Browser To Home Page
    Open Browser    ${Url site}   ${Browser}
    Click Element    ${Home Page}
    Page Should Contain    ${Home page Label}
    
Go to About and Request Demo
    Click Element    ${About Page}
    Page Should Contain    ${AboutUs page Label}
    Click Element    ${Request Demo page}
    Page Should Contain    ${Request page Button}
    
Input required fields to register
    Clear Element Text    ${Input FirstName}
    Input text    ${Input FirstName}    ${FirstName text}
    Clear Element Text    ${Input LastName}
    Input text    ${Input LastName}    ${LastName text}
    Clear Element Text    ${Input Email}
    Input text    ${Input Email}    ${Email text}
    Clear Element Text    ${Input Company}
    Input text    ${Input Company}    ${Company text}
    Clear Element Text    ${Input Companytype}
    Input text    ${Input Companytype}    ${CompanyType text}
    Clear Element Text    ${Input Phone}
    Input text    ${Input Phone}    ${Phone text}
    
Click Submit Button
    Page Should Not Contain    ${Error message}
    Click Element    ${Submit Button}
    
Verify Page redirection after submit
    Page Should contain    ${Page Redirection Thank you}
    
    